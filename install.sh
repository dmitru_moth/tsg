#!/usr/bin/env sh

python3 -m venv tsg \
    && . tsg/bin/activate \
    && pip3 install git+https://gitlab.com/dmitru_moth/tsg.git
