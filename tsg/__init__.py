import logging
import os
import subprocess

from flask import Blueprint, Flask, current_app, request
from werkzeug.exceptions import Forbidden, InternalServerError

bp = Blueprint("v1", __name__)


def create_app() -> Flask:
    logging.basicConfig(
        format="[%(asctime)s] - %(levelname)s - %(name)s - %(message)s",
        level=logging.INFO,
    )

    app = Flask(__name__)

    with app.app_context():
        app.config["tokens"] = os.environ.get("TOKENS", "").split(",")

    app.register_blueprint(bp, url_prefix="/v1/<token>/")

    return app


def send_sms(phone_number: str, text: str) -> None:
    current_app.logger.info(
        "Run termux-sms-send: phone_number=%s text=%s", phone_number, text
    )

    proc = subprocess.run(
        ["termux-sms-send", "-n", phone_number],
        capture_output=True,
        text=True,
        input=text,
    )

    if proc.returncode != 0:
        current_app.logger.error(
            "termux-sms-send error: phone_number=%s text=%s returncode=%s stdout=%s stderr=%s",
            proc.returncode,
            phone_number,
            text,
            proc.returncode,
            " ".join(proc.stdout.splitlines()),
            " ".join(proc.stderr.splitlines()),
        )
        raise InternalServerError("Error in termux-sms-send")
    else:
        current_app.logger.info(
            "Sent sms via termux-sms-send: phone_number=%s text=%s", phone_number, text
        )


def handle_send(token: str, phone_number: str, text: str) -> None:
    if not token in current_app.config["tokens"]:
        raise Forbidden("Invalid token")

    send_sms(phone_number, text)


@bp.route("/send/<phone_number>", methods=["GET", "POST"])
def send_phone_number(token: str, phone_number: str):
    handle_send(token, phone_number, request.data.decode())
    return "OK"


@bp.route("/send/<phone_number>/<text>", methods=["GET", "POST"])
def send_phone_number_text(token: str, phone_number: str, text: str):
    handle_send(token, phone_number, text)
    return "OK"
