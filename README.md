# Termux SMS gateway

Readme in progress :smile:

## Installation

### Via script (Easy and fast method)

You can use this command, copy and paste it into termux. **Please, verify the content of [install.sh](install.sh) script!**

``` sh
curl -s https://gitlab.com/dmitru_moth/tsg/-/raw/master/install.sh | bash
```

After this you can run the server via this command:

``` sh
# Split token by ,
export TOKENS="my_awesome_token1,very_hard_pass"
tsg/bin/waitress-serve --call tsg:create_app
```

### Manual

Just a content of [install.sh](install.sh)

``` sh
python3 -m venv tsg
. tsg/bin/activate
pip3 install git+https://gitlab.com/dmitru_moth/tsg.git
```

## Tokens

You can pass a tokens via environment variable `TOKENS`, tokens must be splitted with `,`

``` sh
export TOKENS="123,Mr3ff3Mo"
```

You can generate strong token via `pwgen`:

``` sh
pwgen -s 32 1
```

## API

``` sh
curl http://localhost:8080/v1/MY_TOKEN/send/PHONE_NUMBER/TEXT
# or
curl http://localhost:8080/v1/MY_TOKEN/send/PHONE_NUMBER -H "Content-Type: text/plain" -d "very large text"
```

